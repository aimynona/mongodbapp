# REST endpoints

Example for call to an endpoint (GET): http://localhost:8089/timeEntries

## GET
##### /timeEntry/timeEntries

	Retrieves all existing time entries.

##### /timeEntry/forId/`{timeEntryId`}

	Retrieves specific time entry with specific time entry ID.

##### /timeEntry/`{category`} 

	Retrieves time entries with specific category.

##### /timeEntry/forUser/`{username`}
	
	Retrieves time entries for specified user.
	
##### /timyEntry/`{category`}/`{username`}")

	Retrieves time entries for specified category and specified user.
	
##### /user/`{userId`}

	Retrieves specific user with specific user id.

## POST
##### /timeEntry/
	Saves time entry.
	
##### /user/
	Saves user entity.
	
## PUT

##### /timeEntry/`{timeEntryId`}
	Updates time entry information for existing time entry. 
	Example JSON: 
```json
{
	"id" : "ti-01",
	"title" : "My meeting title",
	"timeFrom" : "2020-09-28T19:35:22.340Z",
	"timeTo" : "2020-09-28T19:35:22.340Z",
	"category" : "Development",
	"description" : "New project kick-off with client",
	"user" : { "_id" : "1", "username" : "max_mustermann", "password" : "password" }
}
```

## DELETE
#### /timeEntry/delete/`{timeEntryId`}
	
	Deletes specific time entry with specific ID.
	
# Roadmap

- [x] MongoDB Setup
- [x] Spring Boot Setup 
- [x] Webflux integration / connection with MongoDB
- [ ] REST endpoints >> Final filters
- [X] Spring Security
- [ ] UI >> 
- [ ] Data pre-fetching via YAML, ~~Lombok~~, Timestamps, Cleanup