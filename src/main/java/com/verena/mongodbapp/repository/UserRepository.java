package com.verena.mongodbapp.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import com.verena.mongodbapp.document.User;

import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends ReactiveMongoRepository<User, String> {

	
	User findByPassword(String password);
	
	Mono<UserDetails> findUsersByUsername(String username);
		
}
