package com.verena.mongodbapp.repository;

import java.util.Date;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.verena.mongodbapp.document.TimeEntry;

import reactor.core.publisher.Flux;

public interface TimeEntryRepository extends ReactiveMongoRepository<TimeEntry, String> {
	
	@Query("{'category' : ?0}")
	Flux<TimeEntry> findTimeEntriesByCategory(String category);
	
	@Query("{'username' : ?0}")
	Flux<TimeEntry> findTimeEntriesByUserId(String username);

	@Query("{'timeFrom' : ?0}")
	Flux<TimeEntry> findTimeEntriesByDate(Date date);
	
	@Query("{'timeFrom' : { $gte: ?0, $lte: ?1 } }")
	Flux<TimeEntry> findTimeEntriesInBetweenDate(Date dateFrom, Date dateTo);
	
	@Query("{'timeFrom' : ?0, 'category' : ?1}")
	Flux<TimeEntry> findTimeEntriesByDateAndCategory(Date date, String category);

	@Query("{'category' : ?0, 'username' : ?1}")
	Flux<TimeEntry> findTimeEntriesByCategoryForCertainUser(String category, String username);

}
