package com.verena.mongodbapp.controller;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.verena.mongodbapp.document.TimeEntry;
import com.verena.mongodbapp.repository.TimeEntryRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
@RequestMapping("/timeEntry")
@CrossOrigin
public class TimeEntryController {

	@Autowired
	public TimeEntryRepository timeEntryRepo;

	@PostMapping
	public Mono<TimeEntry> create(@RequestBody TimeEntry timeEntry) {
		UUID randomId = java.util.UUID.randomUUID();
		timeEntry.setId(randomId.toString());
		
		return timeEntryRepo.save(timeEntry);
	}

	// Show all entries
	@GetMapping("/timeEntries")
	public Flux<TimeEntry> getAll() {
		return timeEntryRepo.findAll();
	}
	

	// Show specific entry with specific ID
	@GetMapping("/forId/{timeEntryId}")
	public Mono<ResponseEntity<TimeEntry>> getTimeEntryById(@PathVariable String timeEntryId) {

		return timeEntryRepo.findById(timeEntryId).map(timeEntry -> ResponseEntity.ok(timeEntry))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	// Show specific entries in between two specified dates
	@GetMapping("/inBetween/{dateFrom}/{dateTo}")
	public Flux<TimeEntry> getTimeEntriesInBetweenDates(@PathVariable long dateFrom, @PathVariable long dateTo ){		
		return timeEntryRepo.findTimeEntriesInBetweenDate(new Date(dateFrom), new Date(dateTo));
	}
	
	// Show time entries for specific user, by username
	@GetMapping("/forUser/{username}")
	public Flux<TimeEntry> getTimeEntriesByUserId(@PathVariable String username){
		
		return timeEntryRepo.findTimeEntriesByUserId(username);
	}
	
	// Show time entries for specific date
	@GetMapping("/forDate/{date}")
	public Flux<TimeEntry> getTimeEntriesByDate(@PathVariable Date date){
		
		return timeEntryRepo.findTimeEntriesByDate(date);
	}
	
	// Show time entries for specific date
	@GetMapping("/forDateAndCategory/{date}")
	public Flux<TimeEntry> getTimeEntriesByDate(@PathVariable Date date, String category){
		
		return timeEntryRepo.findTimeEntriesByDateAndCategory(date, category);
	}
	
	
	// Show entries with specific category
	@GetMapping("/category/{category}")
	public Flux<TimeEntry> getTimeEntriesByCategory(@PathVariable String category){
			
		return timeEntryRepo.findTimeEntriesByCategory(category);
	} 
	
	// Show entries with specific category for certain user
	@GetMapping("/{category}/{username}")
	public Flux<TimeEntry> getTimeEntriesByCategoryForCertainUser(@PathVariable String category, @PathVariable String username){
		
		return timeEntryRepo.findTimeEntriesByCategoryForCertainUser(category, username);
	} 

	// Update existing entries
	@PutMapping("/{timeEntryId}")
	public Mono<ResponseEntity<TimeEntry>> updateTimeEntry(@PathVariable String timeEntryId,
			@RequestBody TimeEntry timeEntry) {

		return timeEntryRepo.findById(timeEntryId).flatMap(selectedTimeEntryFromDB -> {
			selectedTimeEntryFromDB.setTitle(timeEntry.getTitle());
			selectedTimeEntryFromDB.setDescription(timeEntry.getDescription());
			selectedTimeEntryFromDB.setCategory(timeEntry.getCategory());
			selectedTimeEntryFromDB.setTimeFrom(timeEntry.getTimeFrom());
			selectedTimeEntryFromDB.setTimeTo(timeEntry.getTimeTo());

			return timeEntryRepo.save(selectedTimeEntryFromDB);
		}).map(updatedTimeEntry -> ResponseEntity.ok(updatedTimeEntry))
				.defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));

	}
	
	// Delete entry with specific ID
	@DeleteMapping("/delete/{timeEntryId}")
	public Mono<Void> deleteEntryById(@PathVariable String timeEntryId) {
		return timeEntryRepo.deleteById(timeEntryId);
		
	}

}
