package com.verena.mongodbapp.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.verena.mongodbapp.document.User;
import com.verena.mongodbapp.repository.UserRepository;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

	@Autowired
	public UserRepository userRepo;

	@PostMapping
	public Mono<User> create(@RequestBody User user) {

		return userRepo.save(user);
	}

	@CrossOrigin
	@RequestMapping("/auth")
	public Principal user(Principal user) {
		return user;
	}

	// Show entries with specific category
	@GetMapping("/{username}")
	public Mono<UserDetails> getTimeEntriesByCategory(@PathVariable String username) {

		return userRepo.findUsersByUsername(username);
	}

}
