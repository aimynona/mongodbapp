package com.verena.mongodbapp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.web.server.SecurityWebFilterChain;

import com.verena.mongodbapp.repository.UserRepository;

@Configuration
@EnableReactiveMethodSecurity
@EnableWebFluxSecurity
public class ApplicationSecurityConfig {

	@Autowired
	private UserRepository userRepo;

	@Bean
	public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
		http.csrf().disable().cors().and().authorizeExchange().pathMatchers("/user/auth").permitAll().anyExchange().authenticated().and().httpBasic();

		return http.build();
	}

	@Bean
	public ReactiveUserDetailsService userDetailsService() {
		ReactiveUserDetailsService service = (username) -> userRepo.findUsersByUsername(username);
		return service;
	}
}
