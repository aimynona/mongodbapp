package com.verena.mongodbapp;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.verena.mongodbapp.controller.TimeEntryController;
import com.verena.mongodbapp.controller.UserController;
import com.verena.mongodbapp.document.TimeEntry;
import com.verena.mongodbapp.document.User;

import reactor.core.publisher.Flux;

import java.util.Date;

@SpringBootApplication
@EnableReactiveMongoRepositories
public class MongodbappApplication implements CommandLineRunner {

	@Autowired
//	private UserRepository userRepo;
	private UserController userController;
	private BCryptPasswordEncoder pwEnc = new BCryptPasswordEncoder();

	@Autowired
	private TimeEntryController timeController;

	public static void main(String[] args) {
		SpringApplication.run(MongodbappApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		initializeDatabase();
	}
	

	public void initializeDatabase() {
		
		userController.userRepo.deleteAll();
		timeController.timeEntryRepo.deleteAll();

		/* TODO: users should be pre-fetched from YAML */
		User maxMustermann = new User("us-1", "max_mustermann", encryptPassword("password"));
		User johnDoe = new User("us-2", "john_doe", encryptPassword("admin"));
		User janeDoe = new User("us-3", "jane_doe", encryptPassword("idbeholdl"));
		userController.userRepo.saveAll(Flux.just(johnDoe, janeDoe, maxMustermann)).subscribe();

		Date today = new Date(System.currentTimeMillis());
		Date test = new Date();
		test.setMonth(12);
		
		Date firstDate = new Date();
		firstDate.setYear(2023 - 1900);
		firstDate.setMonth(7 - 1);
		firstDate.setHours(12);
		firstDate.setMinutes(30);
		firstDate.setSeconds(00);
		
		
		Date secondDate = new Date();
		secondDate.setYear(2023 - 1900);
		secondDate.setMonth(7 - 1);
		secondDate.setHours(15);
		secondDate.setMinutes(30);
		secondDate.setSeconds(00);
		
		System.out.println("firstDate: " + firstDate + " and secondDate: " + secondDate);
		
		TimeEntry t1 = new TimeEntry("0", "Client Call with HR", firstDate, secondDate, "Kick-Off Meeting", maxMustermann.getUsername(), "Development");
		TimeEntry t4 = new TimeEntry("1", "Steven's Secret Birthday Bash", today,  today, "Birthday Party", maxMustermann.getUsername(), "Other");
		TimeEntry t5 = new TimeEntry("2", "Meeting room 1", today, today, "Job Interview", maxMustermann.getUsername(), "Meeting");

		TimeEntry t2 = new TimeEntry("3", "Client Name 1", today, today, "Controlling", johnDoe.getUsername(), "Meeting");
		TimeEntry t6 = new TimeEntry("4", "Project 3", today, today, "Refinement", johnDoe.getUsername(), "Development");
		TimeEntry t7 = new TimeEntry("5", "Client Name 3", test, test, "Retro Meeting", johnDoe.getUsername(), "Other");

		TimeEntry t3 = new TimeEntry("6", "Quarterly Status: Testing", test, test, "Testing", janeDoe.getUsername(), "Development");
		TimeEntry t8 = new TimeEntry("7", "Techstack Review", today, today, "Tech Research", janeDoe.getUsername(), "Development");
		TimeEntry t9 = new TimeEntry("8", "Escape the room", today, today, "Teambuilding", janeDoe.getUsername(), "Other");

		timeController.timeEntryRepo.saveAll(Flux.just(t1, t2, t3, t4, t5, t6, t7, t8, t9)).subscribe();
		//timeController.timeEntryRepo.findAll().subscribe(System.out::println);
		
		System.out.println("*************************************");
		System.out.println("*************************************");
		
	//	timeController.timeEntryRepo.findTimeEntriesByDate(today).subscribe(System.out::println);
		//timeController.timeEntryRepo.findTimeEntriesByDateAndCategory(test, "Development").subscribe(System.out::println);
	//	timeController.timeEntryRepo.findTimeEntriesInBetweenDate(firstDate, secondDate).subscribe(System.out::println);
	}
	
	private String encryptPassword(String password)
	{
		String idForEncode = "{bcrypt}";
		return idForEncode + pwEnc.encode(password);
	}
}
