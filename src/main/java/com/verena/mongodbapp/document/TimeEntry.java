package com.verena.mongodbapp.document;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Data
@Document
@ToString
public class TimeEntry {

	@Id
	public String id;
	
	@Getter
	@Setter
	public String title;

	@Getter
	@Setter
	public Date timeFrom;

	@Getter
	@Setter
	public Date timeTo;

	@Getter
	@Setter
	public String category;

	@Getter
	@Setter
	public String description;
	public String username;

	public TimeEntry(String id, String title, Date timeFrom, Date timeTo, String description, String username,
			String category) {
		super();
		this.id = id;
		this.title = title;
		this.timeFrom = timeFrom;
		this.timeTo = timeTo;
		this.description = description;
		this.username = username;
		this.category = category;
	}
	
	public TimeEntry() {
		super();
	}
}
